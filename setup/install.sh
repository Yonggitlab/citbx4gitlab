#!/bin/bash -e

CITBX_SRC_DIR=$(readlink -f $(dirname ${BASH_SOURCE[0]})/../)
. "$CITBX_SRC_DIR/ci-toolbox/common.sh"
. "$CITBX_SRC_DIR/ci-toolbox/env-setup/common.sh"

setup_local() {
    _sudo mkdir -p /usr/local/lib/ci-toolbox/{env-setup,3rdparty}
    _sudo cp -v $CITBX_SRC_DIR/ci-scripts/common.sh /usr/local/lib/ci-toolbox/common.sh
    _sudo cp -v $CITBX_SRC_DIR/ci-toolbox/ci-toolbox.sh /usr/local/lib/ci-toolbox/ci-toolbox.sh
    _sudo chmod +x /usr/local/lib/ci-toolbox/ci-toolbox.sh
    _sudo cp -v $CITBX_SRC_DIR/ci-toolbox/env-setup/* /usr/local/lib/ci-toolbox/env-setup/
    _sudo cp -v $CITBX_SRC_DIR/ci-toolbox/3rdparty/bashopts.sh /usr/local/lib/ci-toolbox/3rdparty/
    _sudo cp -v $CITBX_SRC_DIR/wrapper/ci-toolbox /usr/local/bin/
    _sudo mkdir -p /etc/bash_completion.d
    _sudo cp -v $CITBX_SRC_DIR/wrapper/bashcomp /etc/bash_completion.d/ci-toolbox
    if [ -f /usr/local/lib/ci-toolbox/ci-toolbox.properties ]; then
        print_info "Installing the default configuration file into /usr/local/lib/ci-toolbox/ci-toolbox.properties.default"
        _sudo cp -v $CITBX_SRC_DIR/.ci-toolbox.properties /usr/local/lib/ci-toolbox/ci-toolbox.properties.default
    else
        _sudo cp -v $CITBX_SRC_DIR/.ci-toolbox.properties /usr/local/lib/ci-toolbox/ci-toolbox.properties
    fi
}

_which() {
    for p in $(echo "$PATH" | tr : \ ); do
        if [ -x "$p/$1" ]; then
            echo "$p/$1"
            return 0
        fi
    done
    return 1
}

if _which curl > /dev/null; then
    fetch_dist_file() {
        curl -Lso "$1" "$2"
    }
elif _which wget > /dev/null; then
    fetch_dist_file() {
        wget -O "$1" "$2"
    }
fi

install_pkg_deb() {
    _sudo apt install --reinstall --allow-downgrades -y "$@"
}

install_pkg_rpm() {
    # TODO 1: Find a way to add --reinstall option that works on all cases (install & reinstall)
    # TODO 2: Find an equivalent to the apt 'allow-downgrades' option
    _sudo dnf install -y "$@"
}

setup_pkg() {
    local pkg_type=$1
    mkdir -p $CITBX_SRC_DIR/artifacts
    if ! _which unzip > /dev/null; then
        install_pkg_$pkg_type unzip
    fi
    fetch_dist_file $CITBX_SRC_DIR/artifacts/ci-toolbox.zip \
        https://gitlab.com/ercom/citbx4gitlab/-/jobs/artifacts/$CITBX_GIT_REF/download?job=build-package-$CITBX_PACKAGE_CHANNEL-$pkg_type
    unzip -p $CITBX_SRC_DIR/artifacts/ci-toolbox.zip artifacts/ci-toolbox.$pkg_type > $CITBX_SRC_DIR/artifacts/ci-toolbox.$pkg_type
    install_pkg_$pkg_type $CITBX_SRC_DIR/artifacts/ci-toolbox.$pkg_type
}

if [ -n "$1" ]; then
    CITBX_GIT_REF=$1
fi

if [ -n "$2" ]; then
    CITBX_PACKAGE_CHANNEL=$2
fi

# Assure backward compatibility
if [ "${BASH_SOURCE[0]}" != "$0" ]; then
    if [ "$CITBX_SETUP_TYPE" != "ci-tools" ]; then
        print_critical "This install tool cannot be used for this action anymore (setup type: $CITBX_SETUP_TYPE)"
    fi
    if [ -z "$CITBX_GIT_REF" ] && [ -n "$version" ]; then
        CITBX_GIT_REF=$version
    fi
fi

if [ -z "$CITBX_GIT_REF" ]; then
    print_critical "No version given: this tool requires the first argument to be the version of this tool to install" \
                " or 'local' keyword to force the installation into /usr/local using the generic install process (without using the suited package)" \
                "  You can add the 'dev' keyword as the second argument to use the development package version" "" \
                "Usage: ${BASH_SOURCE[0]} local|master|<version> [dev|development]"
fi

case "$CITBX_PACKAGE_CHANNEL" in
    ''|rel|release)
        CITBX_PACKAGE_CHANNEL=rel
    ;;
    dev|development)
        CITBX_PACKAGE_CHANNEL=dev
    ;;
    *)
        print_critical "Invalid package '$CITBX_PACKAGE_CHANNEL' channel (accepted values are rel|release or dev|development)"
    ;;
esac

if [ "$CITBX_GIT_REF" == "local" ]; then
    setup_local
elif [ -f /etc/debian_version ]; then
    # Debian based system
    setup_pkg deb
elif [ -f /etc/redhat-release ]; then
    # Redhat based system
    setup_pkg rpm
else
    print_warning "No package available for your OS" "> the generic install process ('setup/install.sh local') will be executed"
    setup_local
fi
